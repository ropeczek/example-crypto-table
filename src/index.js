import React from 'react';
import ReactDOM from 'react-dom';
import App from 'View/Root/Root';

ReactDOM.render(
    <App />,
  document.getElementById('root')
);