import React, { useState } from 'react';
import styles from 'View/Root/index.module.scss';
import Table from 'components/Table/Table.js'
import Button from 'components/Button/Button';
import ModalWindow from 'components/ModalWindow/ModalWindow';

const Root = () => {
  const [openModal, setOpenModal] = useState(false);

  const switchModal = () => {
    setOpenModal(openModal => !openModal);
  }

  const addRecord = e => {
    e.preventDefault();
    alert('Request for api');
  };

  const fnDeclarations = {
    switchModal: switchModal,
    addRecord: addRecord,
  }

  return (
    <>
      <div className={styles.container}>
        <Table />
        <Button fn={switchModal}>
          Add Crypto
        </Button>
      </div>
      {/* tutaj lepszym rozwiązaniem byłoby użyć providera, aby do form łatwo szło przekazać funkcje ale juz nie komplikowałem */}
      { openModal && <ModalWindow fns={fnDeclarations} />}
    </>
  )
}

export default Root;
