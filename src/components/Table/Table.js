import React from 'react';
import BodyTable from 'components/Table/BodyTable';
import TableIcons from 'components/Table/TableIcons.js';
import GetData from 'components/GetData/GetData.js';

const Table = () => {

    let { data, error } = GetData();
    if (data !== undefined && !error) {
        data = Object.values(data);
    }

    return (
        <BodyTable
            icons={TableIcons}
            data={data}
            title="Cryptocurrencies"
        />
    )
};

export default Table;