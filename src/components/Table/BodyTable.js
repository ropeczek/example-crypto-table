import MaterialTable from 'material-table';

const BodyTable = ({ icons, data, title }) => (
    <MaterialTable
        icons={icons}
        columns={[{ title: 'Name', field: 'name' },
        { title: 'Unit', field: 'unit' },
        { title: 'Value', field: 'value' },
        { title: 'Type', field: 'type' }]}

        data={data}
        title={title}
    />
);

export default BodyTable;