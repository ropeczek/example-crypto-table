import React from 'react';
import styles from 'components/Button/button.module.scss';

const Button = ({ fn, children }) => (
    <button className={styles.button} onClick={fn}>
        {children}
    </button>
);

export default Button;