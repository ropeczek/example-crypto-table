import React from 'react';
import styles from 'components/ModalWindow/ModalWindow.module.scss';
import Form from 'components/Form/Form';

const ModalWindow = ({ fns }) => {
    const {switchModal, addRecord} = fns;

    return (
        <div className={styles.wrapper}>
            <Form addRecord={addRecord} />
            <button className={styles.closeBtn} onClick={switchModal}>X</button>
        </div>
    );
};

export default ModalWindow;