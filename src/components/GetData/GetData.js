import { useState, useEffect } from 'react';
import axios from 'axios';

const GetData = () => {
    const [data, setData] = useState();
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        const fetch = async () => {
            setIsError(false);
            try {
                const result = await axios('https://api.coingecko.com/api/v3/exchange_rates',);
                setData(JSON.parse(JSON.stringify(result.data.rates)));
            } catch (error) {
                setIsError(true);
                setData(isError);
            }
        }

        fetch();
    }, [isError]);

    return { data: data, error: isError };
}

export default GetData;