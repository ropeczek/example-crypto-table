import React from 'react';
import styles from 'components/Form/form.module.scss';
import Input from 'components/Form/Input';
import Button from 'components/Button/Button';

const Form = ({addRecord}) => (
    <form autoComplete="off">
        <Input placeholder="Name of crypto" name="name" label="Name" type="text" />
        <Input placeholder="Unit" name="unit" label="Unit" type="text" />
        <Input placeholder="Value" name="name" label="Value" type="text" />
        <Input placeholder="Type" name="name" label="Type" type="text" />
        <Button fn={addRecord}>
            Add
        </Button>
    </form>
);

export default Form;