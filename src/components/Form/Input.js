import React from 'react';
import PropTypes from 'prop-types';
import styles from 'components/Form/input.module.scss';
import { useState } from 'react';

const Input = ({ placeholder, name, label, type }) => {
    const [inputValue, setInputValue] = useState('');

    const changeValue = e => {
        setInputValue(e.target.value);
    }

    return (
        <>
            {
                label !== false ? (
                    <label htmlFor={name}>
                        {label}
                    </label>
                ) : null
            }

            <input
                type={type}
                value={inputValue}
                onChange={changeValue}
                placeholder={placeholder}
                name={name}
                id={name} />
        </>
    );
};

//propsy dodane tylko tutaj dla przykładu.
Input.propTypes = {
    placeholder: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    type: PropTypes.string,
};

export default Input;